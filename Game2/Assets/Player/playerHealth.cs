﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class playerHealth : MonoBehaviour
{
    public float fullHealth;
    float curronHealth;

    public  GameObject playerDeadthFX;

    public Slider playerHealthSlider;
    public Image damageScreen;
    Color flashColor = new Color(255f,255f,255f,1f);
    float flashSpeed = 5f;
    bool damaged = false;


    AudioSource playerAS;
    // Start is called before the first frame update
    void Start()
    {
        curronHealth = fullHealth;  
        playerHealthSlider.maxValue = fullHealth;
        playerHealthSlider.value = curronHealth;

        playerAS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (damaged)
        {
            damageScreen.color = flashColor;
        }
        else 
        {
            damageScreen.color = Color.Lerp(damageScreen.color, Color.clear, flashSpeed*Time.deltaTime);
        }
        damaged = false;
    }
    public void addDamage(float damage){
        curronHealth -= damage;
        playerHealthSlider.value = curronHealth;
        damaged = true;

        playerAS.Play();
        if(curronHealth <= 0){
            makeDead();
        }
    }

    public void addHealth(float health)
    {
        curronHealth += health;
        if(curronHealth > fullHealth) curronHealth = fullHealth;
        playerHealthSlider.value = curronHealth;
    }

    public void makeDead(){
        Instantiate(playerDeadthFX, transform.position, Quaternion.Euler(new Vector3(-90,0,0)));
        damageScreen.color = flashColor;
        Destroy(gameObject);
    }
}
