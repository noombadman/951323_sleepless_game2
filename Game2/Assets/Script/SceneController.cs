﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneController : MonoBehaviour
{
    [SerializeField] Button _gamePlay1;
    [SerializeField] Button _gamePlay2;
    [SerializeField] Button _gamePlay3;
    // Start is called before the first frame update
    void Start()
    {
        _gamePlay1.onClick.AddListener
        (delegate{Stage1ButtonClick(_gamePlay1);});
        _gamePlay2.onClick.AddListener
        (delegate{Stage2ButtonClick(_gamePlay2);});
        _gamePlay3.onClick.AddListener
        (delegate{Stage3ButtonClick(_gamePlay3);});
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Stage1ButtonClick(Button button)
    {
        SceneManager.LoadScene("Scenegameplay1");
    }
    public void Stage2ButtonClick(Button button)
    {
        SceneManager.LoadScene("Scenegameplay2");
    }
    public void Stage3ButtonClick(Button button)
    {
        SceneManager.LoadScene("Scenegameplay3");
    }
}
