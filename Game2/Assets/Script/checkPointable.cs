﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkPointable : MonoBehaviour
{
    public Transform currentCheckpoint;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.transform.position = currentCheckpoint.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
