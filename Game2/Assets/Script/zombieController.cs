﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieController : MonoBehaviour
{
    public GameObject flipModel;

    //Audio
    public AudioClip[] idleSounds;
    public float idleSoundTime;
    AudioSource enemyMovemantAS;
    float nextIdleSound = 0f;

    public float detectionTime;
    float startRun;
    bool firstDetaction;

    //movement
    public float runSpeed;
    public float walkSpeed;
    public bool facingRight = true;

    float moveSpeed;
    bool running;

    Rigidbody rb;
    Animator myAnim;
    Transform detectedPlayer;

    bool Detected;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        myAnim = GetComponentInParent<Animator>();
        enemyMovemantAS = GetComponent<AudioSource>();

        running = false;
        Detected = false;
        firstDetaction = false;
        moveSpeed = walkSpeed;

        if(Random.Range(0,10) > 5) Flip();
    }

    // Update is called once per frame
    void FlipUpdate()
    {
        if(Detected)
        {
            if(detectedPlayer.position.x < transform.position.x && facingRight) Flip();
            else if(detectedPlayer.position.x > transform.position.x && !facingRight) Flip();

            if(!firstDetaction)
            {
                startRun = Time.time + detectionTime;
                firstDetaction = true;
            }
        }
        if(Detected && !facingRight)
        rb.velocity = new Vector3(moveSpeed *-1, rb.velocity.y,0);
        
        else if(Detected && facingRight)
        rb.velocity = new Vector3(moveSpeed, rb.velocity.y,0);
        

        if (!running && Detected){
            if(startRun<Time.time){
                moveSpeed = runSpeed;
                myAnim.SetTrigger("run");
                running = true;
            }
        }

        //idel
        if(!running)
        {
            if(Random.Range(0,10) > 5  && nextIdleSound<Time.time)
            {
                AudioClip tempClip = idleSounds[Random.Range(0, idleSounds.Length)];
                enemyMovemantAS.clip = tempClip;
                enemyMovemantAS.Play ();
                nextIdleSound = idleSoundTime + Time.time;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !Detected)
        {
            Debug.Log("eiei");
            Detected = true;
            detectedPlayer = other.transform;
            myAnim.SetBool("detected",Detected);
            if(detectedPlayer.position.x < transform.position.x && facingRight) Flip();
            else if(detectedPlayer.position.x > transform.position.x && !facingRight) Flip();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            firstDetaction =false;
            if(running)
            {
                myAnim.SetTrigger("run");
                moveSpeed = walkSpeed;
                running = false;
            }
        }
    }

    void Flip() 
        {
            facingRight = !facingRight;
            Vector3 theScale = flipModel.transform.localScale;
            theScale.z *= -1;
            flipModel.transform.localScale = theScale;
        }
    
}
