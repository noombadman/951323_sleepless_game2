﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //move
    public float runSpeed;
    public float walkSpeed;
    bool running;

    Rigidbody rb;
    Animator myAnima;


    bool facingRight;

    //jump
    bool grounded = false;
    Collider[] groundCollision;
    float groundChackRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        myAnima = GetComponent<Animator>();
        facingRight = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void FixedUpdate()
    {
        running = false;

        if(grounded && Input.GetAxis("Jump")>0)
        {   
            grounded = false;
            myAnima.SetBool("grounded",grounded);
            rb.AddForce(new Vector3(0,jumpHeight,0));
        }


        groundCollision = Physics.OverlapSphere(groundCheck.position,groundChackRadius,groundLayer);
        if(groundCollision.Length > 0) grounded = true;
        else grounded = false;

        myAnima.SetBool("grounded",grounded);

        float move = Input.GetAxis("Horizontal");
        myAnima.SetFloat("speed",Mathf.Abs(move));

        float sneaking = Input.GetAxisRaw("Fire3");
        myAnima.SetFloat("sneaking",sneaking); 

        float firing = Input.GetAxis("Fire1");
        myAnima.SetFloat("shooting",firing);

        if((sneaking > 0 || firing > 0) && grounded)
        {
            rb.velocity = new Vector3(move * walkSpeed,rb.velocity.y,0);
        }
        else 
        {
            rb.velocity = new Vector3(move * runSpeed,rb.velocity.y,0);
           if(Mathf.Abs(move) > 0) running = true;
        }

        if(move >0 && !facingRight) Flip();
        else if (move <0 && facingRight) Flip();
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    }

    public float GetFacing()
    {
        if (facingRight) return 1;
        else return -1;
    }

    public bool getRunning()
    {
        return (running);
    }
}
